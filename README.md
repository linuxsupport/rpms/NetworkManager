# NetworkManager

Patched version of NetworkManager-1.34.0-0.3.el8 to overcome https://github.com/systemd/systemd/pull/21558

https://its.cern.ch/jira/browse/LOS-841 for full details

# Building

```
# Create a temp internal dummy repo
[root@lxsoftadm28 mbox]# pwd
/mnt/data1/dist/internal/repos/mbox

# Download the required build dependencies. No chance adding them from a repo. Both archs or koji build will fail
cd /mnt/data1/dist/internal/repos/mbox/
curl https://koji.mbox.centos.org/pkgs/packages/libndp/1.7/6.el8/aarch64/libndp-devel-1.7-6.el8.aarch64.rpm -o libndp-devel-1.7-6.el8.aarch64.rpm
curl  https://koji.mbox.centos.org/pkgs/packages/libteam/1.31/2.el8/aarch64/teamd-devel-1.31-2.el8.aarch64.rpm -o teamd-devel-1.31-2.el8.aarch64.rpm
curl https://koji.mbox.centos.org/pkgs/packages/libndp/1.7/6.el8/x86_64/libndp-devel-1.7-6.el8.x86_64.rpm -o libndp-devel-1.7-6.el8.x86_64.rpm
curl  https://koji.mbox.centos.org/pkgs/packages/libteam/1.31/2.el8/x86_64/teamd-devel-1.31-2.el8.x86_64.rpm -o teamd-devel-1.31-2.el8.x86_64.rpm
createrepo ./

# Add the external repo
koji add-external-repo -t cern8s-build mbox http://linuxsoft.cern.ch/internal/repos/mbox/ --mode=bare

# Regen repo
koji regen-repo cern8s-build
```
